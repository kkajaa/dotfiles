call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-sensible'
Plug 'altercation/vim-colors-solarized'
Plug 'PotatoesMaster/i3-vim-syntax'
Plug 'junegunn/goyo.vim'
call plug#end()

set nocompatible
set showcmd
set shell=zsh
set mouse=a
set list
"set cursorline
set relativenumber
syntax on
set background=light	"Light
"set background=dark	"Dark
let g:solarized_visibility="high"
colorscheme solarized
set linebreak
